package ru.renessans.jvschool.volkov.tm.service;

import ru.renessans.jvschool.volkov.tm.api.repository.IOwnerRepository;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.constant.DataConst;
import ru.renessans.jvschool.volkov.tm.exception.empty.*;
import ru.renessans.jvschool.volkov.tm.exception.incorrect.IncorrectIndexException;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public final class TaskService implements IOwnerService<Task> {

    private final IOwnerRepository<Task> taskRepository;

    public TaskService(final IOwnerRepository<Task> taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(final String userId, final String title, final String description) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        final Task task = new Task(title, description);
        return this.taskRepository.add(userId, task);
    }

    @Override
    public Task updateByIndex(final String userId, final Integer index, final String title, final String description) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IncorrectIndexException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        final Task task = getByIndex(userId, index);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        task.setTitle(title);
        task.setDescription(description);

        return task;
    }

    @Override
    public Task updateById(final String userId, final String id, final String title, final String description) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new EmptyTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new EmptyDescriptionException();

        final Task task = getById(userId, id);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        task.setTitle(title);
        task.setDescription(description);

        return task;
    }

    @Override
    public Task deleteByIndex(final String userId, final Integer index) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IncorrectIndexException();
        return this.taskRepository.removeByIndex(userId, index);
    }

    @Override
    public Task deleteById(final String userId, final String id) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(id)) throw new EmptyIdException();
        return this.taskRepository.removeById(userId, id);
    }

    @Override
    public Task deleteByTitle(final String userId, final String title) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(title)) throw new EmptyTitleException();
        return this.taskRepository.removeByTitle(userId, title);
    }

    @Override
    public Collection<Task> deleteAll(final String userId) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        return this.taskRepository.removeAll(userId);
    }

    @Override
    public Task getByIndex(final String userId, final Integer index) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IncorrectIndexException();
        return this.taskRepository.getByIndex(userId, index);
    }

    @Override
    public Task getById(final String userId, final String id) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(id)) throw new EmptyIdException();
        return this.taskRepository.getById(userId, id);
    }

    @Override
    public Task getByTitle(final String userId, final String title) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        if (Objects.isNull(title)) throw new EmptyTitleException();
        return this.taskRepository.getByTitle(userId, title);
    }

    @Override
    public Collection<Task> getAll(final String userId) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new EmptyUserIdException();
        return this.taskRepository.getAll(userId);
    }

    @Override
    public Collection<Task> assignTestData(final Collection<User> users) {
        if (Objects.isNull(users)) throw new EmptyUserException();

        final List<Task> assignedData = new ArrayList<>();
        users.forEach(user -> {
            final Task task =
                    add(user.getId(), DataConst.TASK_TITLE, DataConst.TASK_DESCRIPTION);
            assignedData.add(task);
        });

        return assignedData;
    }

    @Override
    public Collection<Task> importData(final Collection<Task> tasks) {
        if (ValidRuleUtil.isNullOrEmpty(tasks)) return null;
        return this.taskRepository.load(tasks);
    }

    @Override
    public Collection<Task> getExportData() {
        return this.taskRepository.getAllEntries();
    }

}