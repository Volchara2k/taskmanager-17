package ru.renessans.jvschool.volkov.tm.command.authorized.project;

import ru.renessans.jvschool.volkov.tm.api.service.IAuthService;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractAuthOnlyCommand;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProjectViewByIdCommand extends AbstractAuthOnlyCommand {

    private static final String CMD_PROJECT_VIEW_BY_ID = "project-view-by-id";

    private static final String DESC_PROJECT_VIEW_BY_ID = "просмотреть проект по идентификатору";

    private static final String NOTIFY_PROJECT_VIEW_BY_ID_MSG =
            "Для отображения проекта по идентификатору введите идентификатор проекта из списка.\n";

    @Override
    public String getCommand() {
        return CMD_PROJECT_VIEW_BY_ID;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_PROJECT_VIEW_BY_ID;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_PROJECT_VIEW_BY_ID_MSG);
        final String id = ViewUtil.getInstance().getLine();
        final IAuthService authService = super.serviceLocator.getAuthService();
        final String userId = authService.getUserId();
        final IOwnerService<Project> projectService = super.serviceLocator.getProjectService();
        final Project project = projectService.getById(userId, id);
        ViewUtil.getInstance().print(project);
    }

}