package ru.renessans.jvschool.volkov.tm.repository;

import ru.renessans.jvschool.volkov.tm.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.command.authorized.admin.data.base64.DataBase64ClearCommand;
import ru.renessans.jvschool.volkov.tm.command.authorized.admin.data.base64.DataBase64ExportCommand;
import ru.renessans.jvschool.volkov.tm.command.authorized.admin.data.base64.DataBase64ImportCommand;
import ru.renessans.jvschool.volkov.tm.command.authorized.admin.data.binary.DataBinaryClearCommand;
import ru.renessans.jvschool.volkov.tm.command.authorized.admin.usermanage.UserDeleteCommand;
import ru.renessans.jvschool.volkov.tm.command.authorized.admin.usermanage.UserLockCommand;
import ru.renessans.jvschool.volkov.tm.command.authorized.admin.usermanage.UserUnlockCommand;
import ru.renessans.jvschool.volkov.tm.command.everyone.*;
import ru.renessans.jvschool.volkov.tm.command.authorized.admin.data.binary.DataBinaryExportCommand;
import ru.renessans.jvschool.volkov.tm.command.authorized.admin.data.binary.DataBinaryImportCommand;
import ru.renessans.jvschool.volkov.tm.command.authstate.UserLogOutCommand;
import ru.renessans.jvschool.volkov.tm.command.authstate.UserSignInCommand;
import ru.renessans.jvschool.volkov.tm.command.authstate.UserSignUpCommand;
import ru.renessans.jvschool.volkov.tm.command.authorized.project.*;
import ru.renessans.jvschool.volkov.tm.command.authorized.task.*;
import ru.renessans.jvschool.volkov.tm.command.authorized.profile.PasswordUpdateCommand;
import ru.renessans.jvschool.volkov.tm.command.authorized.profile.ProfileEditCommand;
import ru.renessans.jvschool.volkov.tm.command.authorized.profile.ProfileViewCommand;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.*;
import java.util.stream.Collectors;

public final class CommandRepository implements ICommandRepository {

    private static final List<AbstractCommand> COMMANDS = new ArrayList<>(Arrays.asList(
            new HelpListCommand(), new AppVersionCommand(), new DeveloperAboutCommand(), new SystemInfoCommand(),
            new ArgumentListCommand(), new CommandListCommand(), new ProjectCreateCommand(), new ProjectClearCommand(),
            new ProjectListCommand(), new ProjectUpdateByIndexCommand(), new ProjectUpdateByIdCommand(),
            new ProjectDeleteByIndexCommand(), new ProjectDeleteByIdCommand(), new ProjectDeleteByTitleCommand(),
            new ProjectViewByIndexCommand(), new ProjectViewByIdCommand(), new ProjectViewByTitleCommand(),
            new TaskCreateCommand(), new TaskClearCommand(), new TaskListCommand(), new TaskUpdateByIndexCommand(),
            new TaskUpdateByIdCommand(), new TaskDeleteByIndexCommand(), new TaskDeleteByIdCommand(),
            new TaskDeleteByTitleCommand(), new TaskViewByIndexCommand(), new TaskViewByIdCommand(),
            new TaskViewByTitleCommand(), new UserSignInCommand(), new UserSignUpCommand(), new UserLogOutCommand(),
            new ProfileEditCommand(), new PasswordUpdateCommand(), new ProfileViewCommand(),
            new UserLockCommand(), new UserUnlockCommand(), new UserDeleteCommand(), new DataBinaryExportCommand(),
            new DataBinaryImportCommand(), new DataBinaryClearCommand(), new DataBase64ExportCommand(),
            new DataBase64ImportCommand(), new DataBase64ClearCommand(), new AppExitCommand())
    );

    private final Map<String, AbstractCommand> commandMap = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> argumentMap = new LinkedHashMap<>();

    {
        allTerminalCommands().forEach(command -> commandMap.put(command.getCommand(), command));
        allArgumentCommands().forEach(command -> argumentMap.put(command.getArgument(), command));
    }

    @Override
    public Collection<AbstractCommand> allCommand() {
        return COMMANDS;
    }

    @Override
    public Collection<AbstractCommand> allTerminalCommands() {
        return COMMANDS
                .stream()
                .filter(command -> !ValidRuleUtil.isNullOrEmpty(command.getCommand()))
                .collect(Collectors.toList());
    }

    @Override
    public Collection<AbstractCommand> allArgumentCommands() {
        return COMMANDS
                .stream()
                .filter(command -> !ValidRuleUtil.isNullOrEmpty(command.getArgument()))
                .collect(Collectors.toList());
    }

    @Override
    public AbstractCommand getTerminalCommand(final String command) {
        return this.commandMap.values()
                .stream()
                .filter(cmd -> command.equals(cmd.getCommand()))
                .findAny()
                .orElse(null);
    }

    @Override
    public AbstractCommand getArgumentCommand(final String command) {
        return this.argumentMap.values()
                .stream()
                .filter(cmd -> command.equals(cmd.getArgument()))
                .findAny()
                .orElse(null);
    }

}