package ru.renessans.jvschool.volkov.tm.api.repository;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    Collection<AbstractCommand> allCommand();

    Collection<AbstractCommand> allTerminalCommands();

    Collection<AbstractCommand> allArgumentCommands();

    AbstractCommand getArgumentCommand(String command);

    AbstractCommand getTerminalCommand(String command);

}