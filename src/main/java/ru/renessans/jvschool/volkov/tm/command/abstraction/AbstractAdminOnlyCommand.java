package ru.renessans.jvschool.volkov.tm.command.abstraction;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;

public abstract class AbstractAdminOnlyCommand extends AbstractCommand {

    @Override
    public UserRole[] permissions() {
        return new UserRole[] {UserRole.ADMIN};
    }

}