package ru.renessans.jvschool.volkov.tm.api.service;

import ru.renessans.jvschool.volkov.tm.enumeration.AuthState;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.model.User;

public interface IAuthService {

    String getUserId();

    UserRole getUserRole();

    void verifyPermissions(UserRole[] userRoles);

    AuthState signIn(String login, String password);

    User signUp(String login, String password);

    User signUp(String login, String password, String email);

    User signUp(String login, String password, UserRole role);

    boolean logOut();

}