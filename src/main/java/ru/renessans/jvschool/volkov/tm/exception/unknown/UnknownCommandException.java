package ru.renessans.jvschool.volkov.tm.exception.unknown;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class UnknownCommandException extends AbstractRuntimeException {

    private static final String UNKNOWN_COMMAND = "Ошибка! Неизвестная команда: %s!\n";

    public UnknownCommandException(final String message) {
        super(String.format(UNKNOWN_COMMAND, message));
    }

}