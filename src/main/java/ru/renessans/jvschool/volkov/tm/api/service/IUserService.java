package ru.renessans.jvschool.volkov.tm.api.service;

import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.model.User;

import java.util.Collection;

public interface IUserService {

    User getUserById(String id);

    User getUserByLogin(String login);

    User addUser(String login, String password);

    User addUser(String login, String password, String firstName);

    User addUser(String login, String password, UserRole role);

    User editProfileById(String id, String firstName);

    User editProfileById(String id, String firstName, String lastName);

    User updatePasswordById(String id, String newPassword);

    User lockUserByLogin(String login);

    User unlockUserByLogin(String login);

    User deleteUserById(String id);

    User deleteUserByLogin(String login);

    User deleteByUser(User user);

    Collection<User> initUserTestEntries();

    Collection<User> importData(Collection<User> userList);

    Collection<User> getExportData();

}