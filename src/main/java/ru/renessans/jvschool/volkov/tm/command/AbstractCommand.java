package ru.renessans.jvschool.volkov.tm.command;

import ru.renessans.jvschool.volkov.tm.api.service.IServiceLocator;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    protected AbstractCommand() {
    }

    protected AbstractCommand(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getCommand();

    public abstract String getArgument();

    public abstract String getDescription();

    public UserRole[] permissions() {
        return null;
    }

    public abstract void execute() throws Exception;

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        if (!ValidRuleUtil.isNullOrEmpty(getCommand()))
            result.append("Терминальная команда: ").append(getCommand());
        if (!ValidRuleUtil.isNullOrEmpty(getArgument()))
            result.append(", программный аргумент: ").append(getArgument());
        if (!ValidRuleUtil.isNullOrEmpty(getDescription()))
            result.append("\n\t - ").append(getDescription());
        return result.toString();
    }

}