package ru.renessans.jvschool.volkov.tm.exception;

public abstract class AbstractRuntimeException extends RuntimeException {

    public AbstractRuntimeException(final String message) {
        super(message);
    }

    public AbstractRuntimeException(final Throwable cause) {
        super(cause);
    }

}