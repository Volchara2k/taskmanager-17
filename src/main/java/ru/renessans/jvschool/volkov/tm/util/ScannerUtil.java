package ru.renessans.jvschool.volkov.tm.util;

import ru.renessans.jvschool.volkov.tm.exception.incorrect.IncorrectIndexException;

import java.util.Scanner;

public final class ScannerUtil {

    private static final Scanner SCANNER = new Scanner(System.in);

    public static String getLine() {
        return SCANNER.nextLine();
    }

    public static Integer getInteger() {
        final String line = getLine();
        try {
            return Integer.parseInt(line);
        } catch (final Exception e) {
            throw new IncorrectIndexException(line);
        }
    }

    private ScannerUtil() {
    }

}