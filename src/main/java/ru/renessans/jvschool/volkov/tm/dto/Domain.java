package ru.renessans.jvschool.volkov.tm.dto;

import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.model.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public final class Domain implements Serializable {

    private Collection<Project> projectList = new ArrayList<>();

    private Collection<Task> taskList = new ArrayList<>();

    private Collection<User> userList = new ArrayList<>();

    public Collection<Project> getProjectList() {
        return this.projectList;
    }

    public void setProjectList(final Collection<Project> projectList) {
        this.projectList = projectList;
    }

    public Collection<Task> getTaskList() {
        return this.taskList;
    }

    public void setTaskList(final Collection<Task> taskList) {
        this.taskList = taskList;
    }

    public Collection<User> getUserList() {
        return userList;
    }

    public void setUserList(final Collection<User> userList) {
        this.userList = userList;
    }

}