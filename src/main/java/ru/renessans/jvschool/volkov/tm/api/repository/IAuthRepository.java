package ru.renessans.jvschool.volkov.tm.api.repository;

import ru.renessans.jvschool.volkov.tm.model.User;

public interface IAuthRepository {

    String getUserId();

    void signUp(User user);

    boolean logOut();

}