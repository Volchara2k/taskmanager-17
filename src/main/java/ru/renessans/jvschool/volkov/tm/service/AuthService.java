package ru.renessans.jvschool.volkov.tm.service;

import ru.renessans.jvschool.volkov.tm.api.repository.IAuthRepository;
import ru.renessans.jvschool.volkov.tm.api.service.IAuthService;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.enumeration.AuthState;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.exception.empty.*;
import ru.renessans.jvschool.volkov.tm.exception.security.AccessFailureException;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.HashUtil;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.Arrays;
import java.util.Objects;

public final class AuthService implements IAuthService {

    private final IUserService userService;

    private final IAuthRepository authRepository;

    public AuthService(
            final IAuthRepository authRepository,
            final IUserService userService
    ) {
        this.authRepository = authRepository;
        this.userService = userService;
    }

    @Override
    public String getUserId() {
        return this.authRepository.getUserId();
    }

    @Override
    public UserRole getUserRole() {
        final String userId = this.authRepository.getUserId();
        if (ValidRuleUtil.isNullOrEmpty(userId))
            return UserRole.UNKNOWN;

        final User user = this.userService.getUserById(userId);
        if (Objects.isNull(user)) return UserRole.UNKNOWN;

        return user.getRole();
    }

    @Override
    public void verifyPermissions(final UserRole[] userRoles) {
        if (Objects.isNull(userRoles)) return;

        final UserRole userRole = getUserRole();
        if (Objects.isNull(userRole))
            throw new AccessFailureException(AuthState.USER_NOT_FOUND.getTitle());

        boolean containsTypes = Arrays.asList(userRoles).contains(userRole);
        if (containsTypes) return;

        throw new AccessFailureException(AuthState.NO_ACCESS_RIGHTS.getTitle());
    }

    @Override
    public AuthState signIn(final String login, final String password) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();

        final User user = this.userService.getUserByLogin(login);
        if (Objects.isNull(user)) return AuthState.USER_NOT_FOUND;
        if (user.getLockdown()) return AuthState.LOCKDOWN_PROFILE;

        final String passwordHash = HashUtil.getInstance().saltHashLine(password);
        if (ValidRuleUtil.isNullOrEmpty(passwordHash)) return AuthState.APPLICATION_ERROR;
        if (!passwordHash.equals(user.getPasswordHash())) return AuthState.INVALID_PASSWORD;

        this.authRepository.signUp(user);
        return AuthState.SUCCESS;
    }

    @Override
    public User signUp(final String login, final String password) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        return this.userService.addUser(login, password);
    }

    @Override
    public User signUp(final String login, final String password, final String email) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        if (ValidRuleUtil.isNullOrEmpty(email)) throw new EmptyEmailException();

        return this.userService.addUser(login, password, email);
    }

    @Override
    public User signUp(final String login, final String password, final UserRole role) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        if (Objects.isNull(role)) throw new EmptyUserRoleException();

        return this.userService.addUser(login, password, role);
    }

    @Override
    public boolean logOut() {
        return this.authRepository.logOut();
    }

}