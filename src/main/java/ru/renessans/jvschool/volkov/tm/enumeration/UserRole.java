package ru.renessans.jvschool.volkov.tm.enumeration;

public enum UserRole {

    ADMIN("Администратор"),

    USER("Пользователь"),

    UNKNOWN("Неизвестный");

    private final String title;

    UserRole(final String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public boolean isAdmin() {
        return this == ADMIN;
    }

    public boolean isUser() {
        return this == USER;
    }

}