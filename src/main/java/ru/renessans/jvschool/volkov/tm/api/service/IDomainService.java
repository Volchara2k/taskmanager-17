package ru.renessans.jvschool.volkov.tm.api.service;

import ru.renessans.jvschool.volkov.tm.dto.Domain;

public interface IDomainService {

    void dataImport(Domain domain);

    void dataExport(Domain domain);

}