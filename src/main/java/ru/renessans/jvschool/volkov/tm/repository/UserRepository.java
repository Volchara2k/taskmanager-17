package ru.renessans.jvschool.volkov.tm.repository;

import ru.renessans.jvschool.volkov.tm.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyUserException;
import ru.renessans.jvschool.volkov.tm.model.User;

import java.util.*;

public final class UserRepository implements IUserRepository {

    private final Map<String, User> userMap = new LinkedHashMap<>();

    @Override
    public User add(final User user) {
        this.userMap.put(user.getId(), user);
        return user;
    }

    @Override
    public User getById(final String id) {
        return getExportData()
                .stream()
                .filter(user -> id.equals(user.getId()))
                .findAny()
                .orElse(null);
    }

    @Override
    public User getByLogin(final String login) {
        return getExportData()
                .stream()
                .filter(user -> login.equals(user.getLogin()))
                .findAny()
                .orElse(null);
    }

    @Override
    public User deleteById(final String id) {
        final User user = getById(id);
        if (Objects.isNull(user)) throw new EmptyUserException();
        return deleteByUser(user);
    }

    @Override
    public User deleteByLogin(final String login) {
        final User user = getByLogin(login);
        if (Objects.isNull(user)) throw new EmptyUserException();
        return deleteByUser(user);
    }

    @Override
    public User deleteByUser(final User user) {
        this.userMap.values().remove(user);
        return user;
    }

    @Override
    public Collection<User> load(final Collection<User> users) {
        clearEntries();
        users.forEach(this::add);
        return getExportData();
    }

    @Override
    public Collection<User> getExportData() {
        return new ArrayList<>(this.userMap.values());
    }

    @Override
    public void clearEntries() {
        this.userMap.clear();
    }

}