package ru.renessans.jvschool.volkov.tm.constant;

public interface DataConst {

    String TASK_TITLE = "exceptions change";

    String TASK_DESCRIPTION = "change the approach to exceptions";

    String PROJECT_TITLE = "rethink view";

    String PROJECT_DESCRIPTION = "rethink the view approach";

    String USER_TEST_LOGIN = "test";

    String USER_TEST_PASSWORD = "test";

    String USER_DEFAULT_LOGIN = "user";

    String USER_DEFAULT_PASSWORD = "user";

    String USER_DEFAULT_FIRSTNAME = "user";

    String USER_ADMIN_LOGIN = "admin";

    String USER_ADMIN_PASSWORD = "admin";

    String EXIT_FACTOR = "exit";

}