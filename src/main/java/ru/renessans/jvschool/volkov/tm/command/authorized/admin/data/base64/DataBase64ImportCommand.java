package ru.renessans.jvschool.volkov.tm.command.authorized.admin.data.base64;

import ru.renessans.jvschool.volkov.tm.api.service.IDomainService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.DataInterchangeUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Collection;
import java.util.Objects;

public final class DataBase64ImportCommand extends AbstractDataCommand {

    private static final String CMD_BASE64_IMPORT = "data-base64-import";

    private static final String DESC_BASE64_IMPORT = "импортировать домен из base64 вида";

    private static final String NOTIFY_BASE64_IMPORT = "Происходит процесс загрузки домена из base64 вида...";

    @Override
    public String getCommand() {
        return CMD_BASE64_IMPORT;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_BASE64_IMPORT;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.getInstance().print(NOTIFY_BASE64_IMPORT);

        final Domain domain;
        try (final DataInterchangeUtil dataInterchangeUtil = new DataInterchangeUtil()){
            domain = dataInterchangeUtil.readDomainFromBase64(BASE64_FILE_LOCATE);
        }

        if (Objects.isNull(domain)) return;
        final IDomainService domainService = super.serviceLocator.getDomainService();
        domainService.dataImport(domain);

        final Collection<User> users = domain.getUserList();
        ViewUtil.getInstance().print(users);
        final Collection<Task> tasks = domain.getTaskList();
        ViewUtil.getInstance().print(tasks);
        final Collection<Project> projects = domain.getProjectList();
        ViewUtil.getInstance().print(projects);
    }

}