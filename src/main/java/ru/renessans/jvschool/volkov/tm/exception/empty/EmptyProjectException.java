package ru.renessans.jvschool.volkov.tm.exception.empty;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class EmptyProjectException extends AbstractRuntimeException {

    private static final String EMPTY_PROJECT = "Ошибка! Параметр \"проект\" является null!\n";

    public EmptyProjectException() {
        super(EMPTY_PROJECT);
    }

}