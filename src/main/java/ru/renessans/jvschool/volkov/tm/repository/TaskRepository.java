package ru.renessans.jvschool.volkov.tm.repository;

import ru.renessans.jvschool.volkov.tm.api.repository.IOwnerRepository;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyTaskException;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.*;
import java.util.stream.Collectors;

public final class TaskRepository implements IOwnerRepository<Task> {

    private final Map<String, Task> taskMap = new LinkedHashMap<>();

    @Override
    public Task add(final String userId, final Task task) {
        task.setUserId(userId);
        this.taskMap.put(task.getId(), task);
        return task;
    }

    @Override
    public Task removeByIndex(final String userId, final Integer index) {
        final Task task = getByIndex(userId, index);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        return remove(userId, task);
    }

    @Override
    public Task removeById(final String userId, final String id) {
        final Task task = getById(userId, id);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        return remove(userId, task);
    }

    @Override
    public Task removeByTitle(final String userId, final String title) {
        final Task task = getByTitle(userId, title);
        if (Objects.isNull(task)) throw new EmptyTaskException();
        return remove(userId, task);
    }

    @Override
    public Task remove(final String userId, final Task task) {
        this.taskMap.remove(task.getId());
        return task;
    }

    @Override
    public Collection<Task> removeAll(final String userId) {
        final List<Task> removedTasks = (List<Task>) getAll(userId);
        this.taskMap.entrySet().removeIf(taskEntry -> userId.equals(taskEntry.getValue().getUserId()));
        return removedTasks;
    }

    @Override
    public Task getByIndex(final String userId, final Integer index) {
        final List<Task> userTasks = (List<Task>) getAll(userId);
        if (ValidRuleUtil.isNullOrEmpty(userTasks)) return null;
        return userTasks.get(index);
    }

    @Override
    public Task getById(final String userId, final String id) {
        return getAll(userId)
                .stream()
                .filter(task -> id.equals(task.getId()))
                .findAny()
                .orElse(null);
    }

    @Override
    public Task getByTitle(final String userId, final String title) {
        return getAll(userId)
                .stream()
                .filter(task -> title.equals(task.getTitle()))
                .findAny()
                .orElse(null);
    }

    @Override
    public Collection<Task> getAll(final String userId) {
        return getAllEntries()
                .stream()
                .filter(task -> userId.equals(task.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public Collection<Task> load(final Collection<Task> tasks) {
        clearData();
        tasks.forEach(task -> add(task.getUserId(), task));
        return getAllEntries();
    }

    @Override
    public void clearData() {
        this.taskMap.clear();
    }

    @Override
    public Collection<Task> getAllEntries() {
        return new ArrayList<>(this.taskMap.values());
    }

}