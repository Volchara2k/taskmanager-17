package ru.renessans.jvschool.volkov.tm.command.authorized.admin.data.binary;

import ru.renessans.jvschool.volkov.tm.api.service.IDomainService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.DataInterchangeUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Collection;
import java.util.Objects;

public final class DataBinaryImportCommand extends AbstractDataCommand {

    private static final String CMD_BIN_IMPORT = "data-bin-import";

    private static final String DESC_BIN_IMPORT = "импортировать домен из бинарного вида";

    private static final String NOTIFY_BIN_IMPORT = "Происходит процесс загрузки домена из бинарного вида...";

    @Override
    public String getCommand() {
        return CMD_BIN_IMPORT;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_BIN_IMPORT;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.getInstance().print(NOTIFY_BIN_IMPORT);

        final Domain domain;
        try (final DataInterchangeUtil dataInterchangeUtil = new DataInterchangeUtil()){
            domain = dataInterchangeUtil.readDomainFromBin(BIN_FILE_LOCATE);
        }

        if (Objects.isNull(domain)) return;
        final IDomainService domainService = super.serviceLocator.getDomainService();
        domainService.dataImport(domain);

        final Collection<User> users = domain.getUserList();
        ViewUtil.getInstance().print(users);
        final Collection<Task> tasks = domain.getTaskList();
        ViewUtil.getInstance().print(tasks);
        final Collection<Project> projects = domain.getProjectList();
        ViewUtil.getInstance().print(projects);
    }

}