package ru.renessans.jvschool.volkov.tm.command.authorized.profile;

import ru.renessans.jvschool.volkov.tm.api.service.IAuthService;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractAuthOnlyCommand;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class PasswordUpdateCommand extends AbstractAuthOnlyCommand {

    private static final String CMD_UPDATE_PASSWORD = "update-password";

    private static final String ARG_UPDATE_PASSWORD = "обновить пароль пользователя";

    private static final String NOTIFY_UPDATE_PASSWORD = "Для смены пароля введите новый пароль: \n";

    @Override
    public String getCommand() {
        return CMD_UPDATE_PASSWORD;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return ARG_UPDATE_PASSWORD;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_UPDATE_PASSWORD);
        final String password = ViewUtil.getInstance().getLine();
        final IAuthService authService = super.serviceLocator.getAuthService();
        final String userId = authService.getUserId();
        final IUserService userService = super.serviceLocator.getUserService();
        final User user = userService.updatePasswordById(userId, password);
        ViewUtil.getInstance().print(user);
    }

}