package ru.renessans.jvschool.volkov.tm.command.authorized.project;

import ru.renessans.jvschool.volkov.tm.api.service.IAuthService;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractAuthOnlyCommand;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Collection;

public final class ProjectListCommand extends AbstractAuthOnlyCommand {

    private static final String CMD_PROJECT_LIST = "project-list";

    private static final String DESC_PROJECT_LIST = "вывод списка проектов";

    private static final String NOTIFY_PROJECT_LIST = "Текущий список проектов: \n";

    @Override
    public String getCommand() {
        return CMD_PROJECT_LIST;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_PROJECT_LIST;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_PROJECT_LIST);
        final IAuthService authService = super.serviceLocator.getAuthService();
        final String userId = authService.getUserId();
        final IOwnerService<Project> projectService = super.serviceLocator.getProjectService();
        final Collection<Project> projects = projectService.getAll(userId);
        ViewUtil.getInstance().print(projects);
    }

}