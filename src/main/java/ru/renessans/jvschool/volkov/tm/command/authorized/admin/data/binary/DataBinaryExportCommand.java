package ru.renessans.jvschool.volkov.tm.command.authorized.admin.data.binary;

import ru.renessans.jvschool.volkov.tm.api.service.IDomainService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.dto.Domain;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.DataInterchangeUtil;
import ru.renessans.jvschool.volkov.tm.util.FileUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.io.File;
import java.util.List;

public final class DataBinaryExportCommand extends AbstractDataCommand {

    private static final String CMD_BIN_EXPORT = "data-bin-export";

    private static final String DESC_BIN_EXPORT = "экспортировать домен в бинарный вид";

    private static final String NOTIFY_BIN_EXPORT = "Происходит процесс выгрузки домена в бинарный вид...";

    @Override
    public String getCommand() {
        return CMD_BIN_EXPORT;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_BIN_EXPORT;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.getInstance().print(NOTIFY_BIN_EXPORT);

        final Domain domain = new Domain();
        final IDomainService domainService = serviceLocator.getDomainService();
        domainService.dataExport(domain);

        final File file = FileUtil.getInstance().createFile(BIN_FILE_LOCATE);
        try (final DataInterchangeUtil dataInterchangeUtil = new DataInterchangeUtil()) {
            dataInterchangeUtil.writeDomainToBin(domain, file);
        }

        final List<User> users = (List<User>) domain.getUserList();
        ViewUtil.getInstance().print(users);
        final List<Task> tasks = (List<Task>) domain.getTaskList();
        ViewUtil.getInstance().print(tasks);
        final List<Project> projects = (List<Project>) domain.getProjectList();
        ViewUtil.getInstance().print(projects);
    }

}