package ru.renessans.jvschool.volkov.tm.api.repository;

import java.util.Collection;

public interface IOwnerRepository<T> {

    T add(String userId, T object);

    T removeByIndex(String userId, Integer index);

    T removeById(String userId, String id);

    T removeByTitle(String userId, String title);

    T remove(String userId, T object);

    Collection<T> removeAll(String userId);

    T getByIndex(String userId, Integer index);

    T getById(String userId, String id);

    T getByTitle(String userId, String title);

    Collection<T> getAll(String userId);

    Collection<T> load(Collection<T> objects);

    Collection<T> getAllEntries();

    void clearData();

}