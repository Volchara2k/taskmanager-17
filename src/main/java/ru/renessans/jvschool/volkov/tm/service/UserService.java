package ru.renessans.jvschool.volkov.tm.service;

import ru.renessans.jvschool.volkov.tm.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.constant.DataConst;
import ru.renessans.jvschool.volkov.tm.enumeration.UserRole;
import ru.renessans.jvschool.volkov.tm.exception.empty.*;
import ru.renessans.jvschool.volkov.tm.exception.security.AccessFailureException;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.HashUtil;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.*;

public final class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getUserById(final String id) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyUserIdException();
        return this.userRepository.getById(id);
    }

    @Override
    public User getUserByLogin(final String login) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        return this.userRepository.getByLogin(login);
    }

    @Override
    public Collection<User> getExportData() {
        return this.userRepository.getExportData();
    }

    @Override
    public User addUser(final String login, final String password) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();

        final String passwordHash = HashUtil.getInstance().saltHashLine(password);
        if (Objects.isNull(passwordHash)) throw new EmptyHashLineException();

        final User user = new User(login, passwordHash);
        return this.userRepository.add(user);
    }

    @Override
    public User addUser(final String login, final String password, final String firstName) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new EmptyEmailException();

        final String passwordHash = HashUtil.getInstance().saltHashLine(password);
        if (Objects.isNull(passwordHash)) throw new EmptyHashLineException();

        final User user = new User(login, passwordHash, firstName);
        return this.userRepository.add(user);
    }

    @Override
    public User addUser(final String login, final String password, final UserRole role) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new EmptyPasswordException();
        if (Objects.isNull(role)) throw new EmptyUserRoleException();

        final String passwordHash = HashUtil.getInstance().saltHashLine(password);
        if (Objects.isNull(passwordHash)) throw new EmptyHashLineException();

        final User user = new User(login, passwordHash, role);
        return this.userRepository.add(user);
    }

    @Override
    public User updatePasswordById(final String id, final String newPassword) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(newPassword)) throw new EmptyPasswordException();

        final String passwordHash = HashUtil.getInstance().saltHashLine(newPassword);
        if (Objects.isNull(passwordHash)) throw new EmptyHashLineException();

        final User user = this.userRepository.getById(id);
        user.setPasswordHash(passwordHash);

        return user;
    }

    @Override
    public User editProfileById(final String id, final String firstName) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new EmptyFirstNameException();

        final User user = this.userRepository.getById(id);
        user.setFirstName(firstName);

        return user;
    }

    @Override
    public User editProfileById(final String id, final String firstName, final String lastName) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new EmptyFirstNameException();
        if (ValidRuleUtil.isNullOrEmpty(lastName)) throw new EmptyLastNameException();

        final User user = this.userRepository.getById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);

        return user;
    }

    @Override
    public User lockUserByLogin(final String login) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();

        final User user = getUserByLogin(login);
        if (Objects.isNull(user)) throw new EmptyUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException("Операция недоступна!");

        user.setLockdown(true);
        return user;
    }

    @Override
    public User unlockUserByLogin(final String login) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();

        final User user = getUserByLogin(login);
        if (Objects.isNull(user)) throw new EmptyUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException("Операция недоступна!");

        user.setLockdown(false);
        return user;
    }

    @Override
    public User deleteUserById(final String id) {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new EmptyUserIdException();

        final User user = getUserById(id);
        if (Objects.isNull(user)) throw new EmptyUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException("Операция недоступна!");

        return this.userRepository.deleteById(id);
    }

    @Override
    public User deleteUserByLogin(final String login) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new EmptyLoginException();

        final User user = getUserByLogin(login);
        if (Objects.isNull(user)) throw new EmptyUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException("Операция недоступна!");

        return this.userRepository.deleteByLogin(login);
    }

    @Override
    public User deleteByUser(final User user) {
        if (Objects.isNull(user)) throw new EmptyUserException();
        if (user.getRole().isAdmin()) throw new AccessFailureException("Операция недоступна!");
        return this.userRepository.deleteByUser(user);
    }

    @Override
    public Collection<User> initUserTestEntries() {
        return Arrays.asList(
                addUser(DataConst.USER_TEST_LOGIN, DataConst.USER_TEST_PASSWORD),
                addUser(DataConst.USER_DEFAULT_LOGIN, DataConst.USER_DEFAULT_PASSWORD,
                        DataConst.USER_DEFAULT_FIRSTNAME),
                addUser(DataConst.USER_ADMIN_LOGIN, DataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN)
        );
    }

    @Override
    public Collection<User> importData(final Collection<User> userList) {
        if (ValidRuleUtil.isNullOrEmpty(userList)) return null;
        return this.userRepository.load(userList);
    }

}