package ru.renessans.jvschool.volkov.tm.command.everyone;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.util.HardwareDataUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class SystemInfoCommand extends AbstractCommand {

    private static final String CMD_INFO = "info";

    private static final String ARG_INFO = "-i";

    private static final String DESC_INFO = "вывод информации о системе";

    private static final String NOTIFY_INFO = "Информация о системе: \n";

    @Override
    public String getCommand() {
        return CMD_INFO;
    }

    @Override
    public String getArgument() {
        return ARG_INFO;
    }

    @Override
    public String getDescription() {
        return DESC_INFO;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_INFO);
        ViewUtil.getInstance().print(HardwareDataUtil.getInstance().getStatistic());
    }

}