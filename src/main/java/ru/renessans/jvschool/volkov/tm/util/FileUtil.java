package ru.renessans.jvschool.volkov.tm.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public final class FileUtil {

    private static FileUtil INSTANCE;

    public synchronized static FileUtil getInstance() {
        if (Objects.isNull(INSTANCE)) INSTANCE = new FileUtil();
        return INSTANCE;
    }

    private FileUtil() {
    }

    public File createFile(final String filename) throws IOException {
        final File file = new File(filename);
        deleteFile(file.toPath());
        Files.createFile(file.toPath());
        return file;
    }

    public byte[] readFile(final String filename) throws IOException {
        return Files.readAllBytes(Paths.get(filename));
    }

    public boolean deleteFile(final Path path) throws IOException {
        return Files.deleteIfExists(path);
    }

}