package ru.renessans.jvschool.volkov.tm.command.authorized.profile;

import ru.renessans.jvschool.volkov.tm.api.service.IAuthService;
import ru.renessans.jvschool.volkov.tm.api.service.IUserService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractAuthOnlyCommand;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProfileViewCommand extends AbstractAuthOnlyCommand {

    private static final String CMD_VIEW_PROFILE = "view-profile";

    private static final String DESC_VIEW_PROFILE = "обновить пароль пользователя";

    private static final String NOTIFY_VIEW_PROFILE = "Информация о текущем профиле пользователя: \n";

    @Override
    public String getCommand() {
        return CMD_VIEW_PROFILE;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_VIEW_PROFILE;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_VIEW_PROFILE);
        final IAuthService authService = super.serviceLocator.getAuthService();
        final String userId = authService.getUserId();
        final IUserService userService = super.serviceLocator.getUserService();
        final User user = userService.getUserById(userId);
        ViewUtil.getInstance().print(user);
    }

}