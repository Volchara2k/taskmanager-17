package ru.renessans.jvschool.volkov.tm.command.authorized.task;

import ru.renessans.jvschool.volkov.tm.api.service.IAuthService;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractAuthOnlyCommand;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class TaskViewByIdCommand extends AbstractAuthOnlyCommand {

    private static final String CMD_TASK_VIEW_BY_ID = "task-view-by-id";

    private static final String DESC_TASK_VIEW_BY_ID = "просмотреть задачу по идентификатору";

    private static final String NOTIFY_TASK_VIEW_BY_ID =
            "Для отображения задачи по идентификатору введите идентификатор задачи из списка.\n";

    @Override
    public String getCommand() {
        return CMD_TASK_VIEW_BY_ID;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_TASK_VIEW_BY_ID;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_TASK_VIEW_BY_ID);
        final String id = ViewUtil.getInstance().getLine();
        final IAuthService authService = super.serviceLocator.getAuthService();
        final String userId = authService.getUserId();
        final IOwnerService<Task> taskService = super.serviceLocator.getTaskService();
        final Task task = taskService.getById(userId, id);
        ViewUtil.getInstance().print(task);
    }

}