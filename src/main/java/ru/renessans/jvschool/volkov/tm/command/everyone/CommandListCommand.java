package ru.renessans.jvschool.volkov.tm.command.everyone;

import ru.renessans.jvschool.volkov.tm.api.service.ICommandService;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.exception.security.NotifyAddFailureException;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Collection;

public final class CommandListCommand extends AbstractCommand {

    private static final String CMD_COMMANDS = "commands";

    private static final String ARG_COMMANDS = "-cmd";

    private static final String DESC_COMMANDS = "вывод списка поддерживаемых терминальных команд";

    private static final String NOTIFY_COMMANDS = "Список поддерживаемых терминальных команд: \n";

    @Override
    public String getCommand() {
        return CMD_COMMANDS;
    }

    @Override
    public String getArgument() {
        return ARG_COMMANDS;
    }

    @Override
    public String getDescription() {
        return DESC_COMMANDS;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_COMMANDS);
        final ICommandService commandService = super.serviceLocator.getCommandService();
        final Collection<AbstractCommand> commands = commandService.allTerminalCommands();
        if (ValidRuleUtil.isNullOrEmpty(commands))
            throw new NotifyAddFailureException("Терминальные команды");
        ViewUtil.getInstance().print(commands);
    }

}