package ru.renessans.jvschool.volkov.tm.util;

import java.util.Objects;

public final class HardwareDataUtil {

    private static final String SYSTEM_INFO =
            "Доступные процессоры (ядра): %d; \n" +
                    "Свободная память: %s; \n" +
                    "Максимальная память: %s; \n" +
                    "Общая память, доступная JVM: %s; \n" +
                    "Используемая память JVM: %s.";

    private static volatile HardwareDataUtil INSTANCE;

    public static HardwareDataUtil getInstance() {
        final HardwareDataUtil result = INSTANCE;
        if (!Objects.isNull(result)) return result;

        synchronized (HardwareDataUtil.class) {
            if (Objects.isNull(INSTANCE)) INSTANCE = new HardwareDataUtil();
            return INSTANCE;
        }
    }

    private HardwareDataUtil() {
    }

    public String getStatistic() {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = formatBytes(freeMemory);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = formatBytes(totalMemory);
        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = formatBytes(usedMemory);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = formatBytes(maxMemory);
        final String maxMemoryFormat = (maxMemory == Long.MAX_VALUE ? "без ограничений" : maxMemoryValue);

        return String.format(
                SYSTEM_INFO, availableProcessors, freeMemoryFormat, maxMemoryFormat, totalMemoryFormat, usedMemoryFormat
        );
    }

    private String formatBytes(final long bytes) {
        final long kilobyte = 1024;
        final long megabyte = kilobyte * 1024;
        final long gigabyte = megabyte * 1024;
        final long terabyte = gigabyte * 1024;

        if ((bytes >= 0) && (bytes < kilobyte)) {
            return bytes + " B";
        } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
            return (bytes / kilobyte) + " KB";
        } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
            return (bytes / megabyte) + " MB";
        } else if ((bytes >= gigabyte) && (bytes < terabyte)) {
            return (bytes / gigabyte) + " GB";
        } else if (bytes >= terabyte) {
            return (bytes / terabyte) + " TB";
        } else {
            return bytes + " Bytes";
        }
    }

}