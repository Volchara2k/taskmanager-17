package ru.renessans.jvschool.volkov.tm.command.authorized.project;

import ru.renessans.jvschool.volkov.tm.api.service.IAuthService;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractAuthOnlyCommand;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProjectDeleteByTitleCommand extends AbstractAuthOnlyCommand {

    private static final String CMD_PROJECT_DELETE_BY_TITLE = "project-delete-by-title";

    private static final String DESC_PROJECT_DELETE_BY_TITLE = "удалить проект по заголовку";

    private static final String NOTIFY_PROJECT_DELETE_BY_TITLE =
            "Для удаления заголовка по имени введите заголовок проекта из списка.\n";

    @Override
    public String getCommand() {
        return CMD_PROJECT_DELETE_BY_TITLE;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_PROJECT_DELETE_BY_TITLE;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_PROJECT_DELETE_BY_TITLE);
        final String title = ViewUtil.getInstance().getLine();
        final IAuthService authService = super.serviceLocator.getAuthService();
        final String userId = authService.getUserId();
        final IOwnerService<Project> projectService = super.serviceLocator.getProjectService();
        final Project project = projectService.deleteByTitle(userId, title);
        ViewUtil.getInstance().print(project);
    }

}