package ru.renessans.jvschool.volkov.tm.command.authorized.project;

import ru.renessans.jvschool.volkov.tm.api.service.IAuthService;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractAuthOnlyCommand;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProjectViewByIndexCommand extends AbstractAuthOnlyCommand {

    private static final String CMD_PROJECT_VIEW_BY_INDEX = "project-view-by-index";

    private static final String DESC_PROJECT_VIEW_BY_INDEX = "просмотреть проект по индексу";

    private static final String NOTIFY_PROJECT_VIEW_BY_INDEX_MSG =
            "Для отображения проекта по индексу введите индекс проекта из списка.\n";

    @Override
    public String getCommand() {
        return CMD_PROJECT_VIEW_BY_INDEX;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_PROJECT_VIEW_BY_INDEX;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_PROJECT_VIEW_BY_INDEX_MSG);
        final Integer index = ViewUtil.getInstance().getInteger() - 1;
        final IAuthService authService = super.serviceLocator.getAuthService();
        final String userId = authService.getUserId();
        final IOwnerService<Project> projectService = super.serviceLocator.getProjectService();
        final Project project = projectService.getByIndex(userId, index);
        ViewUtil.getInstance().print(project);
    }

}