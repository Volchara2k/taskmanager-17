package ru.renessans.jvschool.volkov.tm.util;

import ru.renessans.jvschool.volkov.tm.enumeration.AuthState;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyProjectException;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyTaskException;
import ru.renessans.jvschool.volkov.tm.exception.security.AccessFailureException;
import ru.renessans.jvschool.volkov.tm.exception.security.UserLogOutFailureException;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.model.User;

import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public final class ViewUtil {

    private static final String SUCCESSFUL_OUTCOME = "\nОперация УСПЕШНО завершилась!\n";

    private static final String UNSUCCESSFUL_OUTCOME = "\nОперация завершилась с ОШИБКОЙ!\n";

    private static volatile ViewUtil INSTANCE;

    public static ViewUtil getInstance() {
        final ViewUtil result = INSTANCE;
        if (!Objects.isNull(result)) return result;

        synchronized (ViewUtil.class) {
            if (Objects.isNull(INSTANCE)) INSTANCE = new ViewUtil();
            return INSTANCE;
        }
    }

    private ViewUtil() {
    }

    public void print(final String string) {
        System.out.println(string);
    }

    public void print(final boolean state) {
        if (!state) {
            System.out.println(UNSUCCESSFUL_OUTCOME);
            throw new UserLogOutFailureException();
        }

        System.out.println(SUCCESSFUL_OUTCOME);
    }

    public void print(final Collection<?> collection) {
        if (ValidRuleUtil.isNullOrEmpty(collection)) {
            System.out.println("Список на текущий момент пуст.");
            return;
        }

        final AtomicInteger index = new AtomicInteger(1);
        collection.forEach(element -> {
            System.out.println(index + ". " + element);
            index.getAndIncrement();
        });

        System.out.println(SUCCESSFUL_OUTCOME);
    }

    public void print(final Project project) {
        if (Objects.isNull(project)) {
            System.out.println(UNSUCCESSFUL_OUTCOME);
            throw new EmptyProjectException();
        }

        System.out.printf("\n%s;\nИдентификатор: %s.\n%s",
                project, project.getId(), SUCCESSFUL_OUTCOME);
    }

    public void print(final AuthState authState) {
        if (Objects.isNull(authState)) {
            System.out.println(UNSUCCESSFUL_OUTCOME);
            throw new AccessFailureException();
        }

        if (!authState.isSuccess()) {
            System.out.println(UNSUCCESSFUL_OUTCOME);
            throw new AccessFailureException(authState.getTitle());
        }

        System.out.println(SUCCESSFUL_OUTCOME);
    }

    public void print(final User user) {
        if (Objects.isNull(user)) {
            System.out.println(UNSUCCESSFUL_OUTCOME);
            throw new EmptyProjectException();
        }

        System.out.printf("\n%s\nИдентификатор: %s.\n%s",
                user, user.getId(), SUCCESSFUL_OUTCOME);
    }

    public void print(final Task task) {
        if (Objects.isNull(task)) {
            System.out.println(UNSUCCESSFUL_OUTCOME);
            throw new EmptyTaskException();
        }

        System.out.printf("\n%s;\nИдентификатор: %s.\n%s",
                task, task.getId(), SUCCESSFUL_OUTCOME);
    }

    public String getLine() {
        print("Введите данные: ");
        return ScannerUtil.getLine();
    }

    public Integer getInteger() {
        print("Введите данные: ");
        return ScannerUtil.getInteger();
    }

}