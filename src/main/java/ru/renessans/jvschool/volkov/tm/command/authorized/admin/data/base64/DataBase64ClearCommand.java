package ru.renessans.jvschool.volkov.tm.command.authorized.admin.data.base64;

import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractDataCommand;
import ru.renessans.jvschool.volkov.tm.util.FileUtil;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.nio.file.Path;
import java.nio.file.Paths;

public final class DataBase64ClearCommand extends AbstractDataCommand {

    private static final String CMD_BASE64_CLEAR = "data-base64-clear";

    private static final String DESC_BASE64_CLEAR = "очистить base64 данные";

    private static final String NOTIFY_BASE64_CLEAR = "Происходит процесс очищения base64 данных...";

    @Override
    public String getCommand() {
        return CMD_BASE64_CLEAR;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_BASE64_CLEAR;
    }

    @Override
    public void execute() throws Exception {
        ViewUtil.getInstance().print(NOTIFY_BASE64_CLEAR);
        final Path path = Paths.get(BASE64_FILE_LOCATE);
        final boolean removeState = FileUtil.getInstance().deleteFile(path);
        ViewUtil.getInstance().print(removeState);
    }

}