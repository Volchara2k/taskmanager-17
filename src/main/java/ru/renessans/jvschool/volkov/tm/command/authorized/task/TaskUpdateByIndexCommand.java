package ru.renessans.jvschool.volkov.tm.command.authorized.task;

import ru.renessans.jvschool.volkov.tm.api.service.IAuthService;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractAuthOnlyCommand;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class TaskUpdateByIndexCommand extends AbstractAuthOnlyCommand {

    private static final String CMD_TASK_UPDATE_BY_INDEX = "task-update-by-index";

    private static final String DESC_TASK_UPDATE_BY_INDEX = "обновить задачу по индексу";

    private static final String NOTIFY_TASK_UPDATE_BY_INDEX =
            "Для обновления задачи по индексу введите индекс задачи из списка.\n" +
                    "Для обновления задачи введите её заголовок или заголовок с описанием.\n";

    @Override
    public String getCommand() {
        return CMD_TASK_UPDATE_BY_INDEX;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_TASK_UPDATE_BY_INDEX;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_TASK_UPDATE_BY_INDEX);
        final Integer index = ViewUtil.getInstance().getInteger() - 1;
        final String title = ViewUtil.getInstance().getLine();
        final String description = ViewUtil.getInstance().getLine();
        final IAuthService authService = super.serviceLocator.getAuthService();
        final String userId = authService.getUserId();
        final IOwnerService<Task> taskService = super.serviceLocator.getTaskService();
        final Task updatedTask = taskService.updateByIndex(userId, index, title, description);
        ViewUtil.getInstance().print(updatedTask);
    }

}