package ru.renessans.jvschool.volkov.tm.model;

import java.util.Objects;

public class Task extends AbstractSerializableModel {

    private static final long serialVersionUID = 1L;

    private String title = "";

    private String description = "";

    private String userId;

    public Task() {
    }

    public Task(final String title) {
        this.title = title;
    }

    public Task(final String title, final String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        if (!Objects.isNull(this.title))
            result.append("Заголовок задачи: ").append(this.title);
        if (!Objects.isNull(this.description))
            result.append(", описание задачи - ").append(this.description);
        return result.toString();
    }

}