package ru.renessans.jvschool.volkov.tm.repository;

import ru.renessans.jvschool.volkov.tm.api.repository.IOwnerRepository;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyProjectException;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.*;
import java.util.stream.Collectors;

public final class ProjectRepository implements IOwnerRepository<Project> {

    private final Map<String, Project> projectMap = new LinkedHashMap<>();

    @Override
    public Project add(final String userId, final Project project) {
        project.setUserId(userId);
        this.projectMap.put(project.getId(), project);
        return project;
    }

    @Override
    public Project removeByIndex(final String userId, final Integer index) {
        final Project project = getByIndex(userId, index);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        return remove(userId, project);
    }

    @Override
    public Project removeById(final String userId, final String id) {
        final Project project = getById(userId, id);
        if (Objects.isNull(project)) return null;
        return remove(userId, project);
    }

    @Override
    public Project removeByTitle(final String userId, final String title) {
        final Project project = getByTitle(userId, title);
        if (Objects.isNull(project)) throw new EmptyProjectException();
        return remove(userId, project);
    }

    @Override
    public Project remove(final String userId, final Project project) {
        this.projectMap.remove(project.getId());
        return project;
    }

    @Override
    public Collection<Project> removeAll(final String userId) {
        final List<Project> removedProjects = (List<Project>) getAll(userId);
        this.projectMap.entrySet().removeIf(taskEntry -> userId.equals(taskEntry.getValue().getUserId()));
        return removedProjects;
    }

    @Override
    public Project getByIndex(final String userId, final Integer index) {
        final List<Project> userProjects = (List<Project>) getAll(userId);
        if (ValidRuleUtil.isNullOrEmpty(userProjects)) return null;
        return userProjects.get(index);
    }

    @Override
    public Project getById(final String userId, final String id) {
        return getAll(userId)
                .stream()
                .filter(task -> id.equals(task.getId()))
                .findAny()
                .orElse(null);
    }

    @Override
    public Project getByTitle(final String userId, final String title) {
        return getAll(userId)
                .stream()
                .filter(task -> title.equals(task.getTitle()))
                .findAny()
                .orElse(null);
    }

    @Override
    public Collection<Project> getAll(final String userId) {
        return getAllEntries()
                .stream()
                .filter(task -> userId.equals(task.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public Collection<Project> load(final Collection<Project> projects) {
        clearData();
        projects.forEach(project -> add(project.getUserId(), project));
        return getAllEntries();
    }

    @Override
    public void clearData() {
        this.projectMap.clear();
    }

    @Override
    public Collection<Project> getAllEntries() {
        return new ArrayList<>(this.projectMap.values());
    }

}