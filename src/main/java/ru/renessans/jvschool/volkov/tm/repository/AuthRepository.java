package ru.renessans.jvschool.volkov.tm.repository;

import ru.renessans.jvschool.volkov.tm.api.repository.IAuthRepository;
import ru.renessans.jvschool.volkov.tm.model.User;

public final class AuthRepository implements IAuthRepository {

    private String userId;

    @Override
    public String getUserId() {
        return this.userId;
    }

    @Override
    public void signUp(final User user) {
        this.userId = user.getId();
    }

    @Override
    public boolean logOut() {
        this.userId = null;
        return true;
    }

}