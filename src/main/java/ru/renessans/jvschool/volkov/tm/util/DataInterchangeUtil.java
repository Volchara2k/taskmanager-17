package ru.renessans.jvschool.volkov.tm.util;

import ru.renessans.jvschool.volkov.tm.dto.Domain;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public final class DataInterchangeUtil implements Closeable {

    private ObjectInputStream objectInputStream = null;

    private ObjectOutputStream objectOutputStream = null;

    private ByteArrayInputStream byteArrayInputStream = null;

    private ByteArrayOutputStream byteArrayOutputStream = null;

    private FileOutputStream fileOutputStream = null;

    @Override
    public void close() throws IOException {
        if (!Objects.isNull(this.objectOutputStream)) this.objectOutputStream.close();
        if (!Objects.isNull(this.objectInputStream)) this.objectInputStream.close();
        if (!Objects.isNull(this.byteArrayOutputStream)) this.byteArrayOutputStream.close();
        if (!Objects.isNull(this.byteArrayInputStream)) this.byteArrayInputStream.close();
        if (!Objects.isNull(this.fileOutputStream)) this.fileOutputStream.close();
    }

    public void writeDomainToBin(final Domain domain, final File file) throws IOException {
        if (Objects.isNull(domain)) return;
        if (Objects.isNull(file)) return;

        this.objectOutputStream = new ObjectOutputStream(new FileOutputStream(file));
        this.objectOutputStream.writeObject(domain);
        this.objectOutputStream.flush();
    }

    public Domain readDomainFromBin(final String file) throws IOException, ClassNotFoundException {
        if (ValidRuleUtil.isNullOrEmpty(file)) return null;
        this.objectInputStream = new ObjectInputStream(new FileInputStream(file));
        return (Domain) this.objectInputStream.readObject();
    }

    public void writeDomainToBase64(final Domain domain, final File file) throws IOException {
        if (Objects.isNull(domain)) return;
        if (Objects.isNull(file)) return;

        this.byteArrayOutputStream = new ByteArrayOutputStream();
        this.objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        this.objectOutputStream.writeObject(domain);
        this.objectOutputStream.flush();

        final byte[] fileBytes = byteArrayOutputStream.toByteArray();
        final String base64 = new BASE64Encoder().encode(fileBytes);
        final byte[] base64Bytes = base64.getBytes(StandardCharsets.UTF_8);

        this.fileOutputStream = new FileOutputStream(file.getName());
        this.fileOutputStream.write(base64Bytes);
        this.fileOutputStream.flush();
    }

    public Domain readDomainFromBase64(final String file) throws IOException, ClassNotFoundException {
        if (ValidRuleUtil.isNullOrEmpty(file)) return null;

        final byte[] fileBytes = FileUtil.getInstance().readFile(file);
        final String base64 = new String(fileBytes);
        final byte[] base64Bytes = new BASE64Decoder().decodeBuffer(base64);

        this.byteArrayInputStream = new ByteArrayInputStream(base64Bytes);
        this.objectInputStream = new ObjectInputStream(byteArrayInputStream);

        return (Domain) this.objectInputStream.readObject();
    }

}