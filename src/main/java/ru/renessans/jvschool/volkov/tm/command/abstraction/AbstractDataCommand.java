package ru.renessans.jvschool.volkov.tm.command.abstraction;

public abstract class AbstractDataCommand extends AbstractAdminOnlyCommand {

    protected static final String BIN_FILE_LOCATE = "./data.bin";

    protected static final String BASE64_FILE_LOCATE = "./data.base64";

}