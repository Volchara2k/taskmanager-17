package ru.renessans.jvschool.volkov.tm.command.authorized.project;

import ru.renessans.jvschool.volkov.tm.api.service.IAuthService;
import ru.renessans.jvschool.volkov.tm.api.service.IOwnerService;
import ru.renessans.jvschool.volkov.tm.command.abstraction.AbstractAuthOnlyCommand;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProjectViewByTitleCommand extends AbstractAuthOnlyCommand {

    private static final String CMD_PROJECT_VIEW_BY_TITLE = "project-view-by-title";

    private static final String DESC_PROJECT_VIEW_BY_TITLE = "просмотреть проект по заголовку";

    private static final String NOTIFY_PROJECT_VIEW_BY_TITLE_MSG =
            "Для отображения проекта по заголовку введите заголовок проекта из списка.\n";

    @Override
    public String getCommand() {
        return CMD_PROJECT_VIEW_BY_TITLE;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_PROJECT_VIEW_BY_TITLE;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_PROJECT_VIEW_BY_TITLE_MSG);
        final String title = ViewUtil.getInstance().getLine();
        final IAuthService authService = super.serviceLocator.getAuthService();
        final String userId = authService.getUserId();
        final IOwnerService<Project> projectService = super.serviceLocator.getProjectService();
        final Project project = projectService.getByTitle(userId, title);
        ViewUtil.getInstance().print(project);
    }

}